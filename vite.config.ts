import { defineConfig } from "vite";
import solid from "vite-plugin-solid";
import dts from "vite-plugin-dts";
import path from "path";

export default defineConfig({
    plugins: [solid(), dts()],
    build: {
        ssr: true,
        lib: {
            entry: path.resolve(__dirname, "src/index.tsx"),
            name: "solid-feather",
            fileName: (format) => `index.${format}.js`,
        },
        rollupOptions: {
            external: ["solid-js", "web"],
            output: {
                globals: {
                    "solid-js/web": "web",
                    "solid-js": "solidJs",
                },
            },
        },
    },
});
