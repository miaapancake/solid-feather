import IconContainer, { IconProps } from "../icon";

const Bookmark = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z" />
        </IconContainer>
    );
};

Bookmark.displayName = "Bookmark";

export default Bookmark;
