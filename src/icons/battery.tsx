import IconContainer, { IconProps } from "../icon";

const Battery = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <rect x="1" y="6" width="18" height="12" rx="2" ry="2" />
            <line x1="23" y1="13" x2="23" y2="11" />
        </IconContainer>
    );
};

Battery.displayName = "Battery";

export default Battery;
