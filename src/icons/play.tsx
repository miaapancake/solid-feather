import IconContainer, { IconProps } from "../icon";

const Play = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polygon points="5 3 19 12 5 21 5 3" />
        </IconContainer>
    );
};

Play.displayName = "Play";

export default Play;
