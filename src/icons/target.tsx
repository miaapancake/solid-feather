import IconContainer, { IconProps } from "../icon";

const Target = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <circle cx="12" cy="12" r="6" />
            <circle cx="12" cy="12" r="2" />
        </IconContainer>
    );
};

Target.displayName = "Target";

export default Target;
