import IconContainer, { IconProps } from "../icon";

const ChevronsLeft = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="11 17 6 12 11 7" />
            <polyline points="18 17 13 12 18 7" />
        </IconContainer>
    );
};

ChevronsLeft.displayName = "ChevronsLeft";

export default ChevronsLeft;
