import IconContainer, { IconProps } from "../icon";

const MoreVertical = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="1" />
            <circle cx="12" cy="5" r="1" />
            <circle cx="12" cy="19" r="1" />
        </IconContainer>
    );
};

MoreVertical.displayName = "MoreVertical";

export default MoreVertical;
