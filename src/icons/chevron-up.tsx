import IconContainer, { IconProps } from "../icon";

const ChevronUp = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="18 15 12 9 6 15" />
        </IconContainer>
    );
};

ChevronUp.displayName = "ChevronUp";

export default ChevronUp;
