import IconContainer, { IconProps } from "../icon";

const Filter = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polygon points="22 3 2 3 10 12.46 10 19 14 21 14 12.46 22 3" />
        </IconContainer>
    );
};

Filter.displayName = "Filter";

export default Filter;
