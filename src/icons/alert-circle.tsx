import IconContainer, { IconProps } from "../icon";

const AlertCircle = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <line x1="12" y1="8" x2="12" y2="12" />
            <line x1="12" y1="16" x2="12.01" y2="16" />
        </IconContainer>
    );
};

AlertCircle.displayName = "AlertCircle";

export default AlertCircle;
