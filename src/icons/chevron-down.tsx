import IconContainer, { IconProps } from "../icon";

const ChevronDown = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="6 9 12 15 18 9" />
        </IconContainer>
    );
};

ChevronDown.displayName = "ChevronDown";

export default ChevronDown;
