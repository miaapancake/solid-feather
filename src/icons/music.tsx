import IconContainer, { IconProps } from "../icon";

const Music = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M9 18V5l12-2v13" />
            <circle cx="6" cy="18" r="3" />
            <circle cx="18" cy="16" r="3" />
        </IconContainer>
    );
};

Music.displayName = "Music";

export default Music;
