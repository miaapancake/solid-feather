import IconContainer, { IconProps } from "../icon";

const FastForward = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polygon points="13 19 22 12 13 5 13 19" />
            <polygon points="2 19 11 12 2 5 2 19" />
        </IconContainer>
    );
};

FastForward.displayName = "FastForward";

export default FastForward;
