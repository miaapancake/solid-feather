import IconContainer, { IconProps } from "../icon";

const Edit2 = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z" />
        </IconContainer>
    );
};

Edit2.displayName = "Edit2";

export default Edit2;
