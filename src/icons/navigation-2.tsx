import IconContainer, { IconProps } from "../icon";

const Navigation2 = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polygon points="12 2 19 21 12 17 5 21 12 2" />
        </IconContainer>
    );
};

Navigation2.displayName = "Navigation2";

export default Navigation2;
