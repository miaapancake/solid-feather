import IconContainer, { IconProps } from "../icon";

const TrendingDown = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="23 18 13.5 8.5 8.5 13.5 1 6" />
            <polyline points="17 18 23 18 23 12" />
        </IconContainer>
    );
};

TrendingDown.displayName = "TrendingDown";

export default TrendingDown;
