import IconContainer, { IconProps } from "../icon";

const Umbrella = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M23 12a11.05 11.05 0 0 0-22 0zm-5 7a3 3 0 0 1-6 0v-7" />
        </IconContainer>
    );
};

Umbrella.displayName = "Umbrella";

export default Umbrella;
