import IconContainer, { IconProps } from "../icon";

const ArrowUpLeft = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <line x1="17" y1="17" x2="7" y2="7" />
            <polyline points="7 17 7 7 17 7" />
        </IconContainer>
    );
};

ArrowUpLeft.displayName = "ArrowUpLeft";

export default ArrowUpLeft;
