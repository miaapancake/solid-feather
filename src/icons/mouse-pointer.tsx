import IconContainer, { IconProps } from "../icon";

const MousePointer = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M3 3l7.07 16.97 2.51-7.39 7.39-2.51L3 3z" />
            <path d="M13 13l6 6" />
        </IconContainer>
    );
};

MousePointer.displayName = "MousePointer";

export default MousePointer;
