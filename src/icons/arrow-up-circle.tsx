import IconContainer, { IconProps } from "../icon";

const ArrowUpCircle = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <polyline points="16 12 12 8 8 12" />
            <line x1="12" y1="16" x2="12" y2="8" />
        </IconContainer>
    );
};

ArrowUpCircle.displayName = "ArrowUpCircle";

export default ArrowUpCircle;
