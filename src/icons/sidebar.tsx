import IconContainer, { IconProps } from "../icon";

const Sidebar = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
            <line x1="9" y1="3" x2="9" y2="21" />
        </IconContainer>
    );
};

Sidebar.displayName = "Sidebar";

export default Sidebar;
