import IconContainer, { IconProps } from "../icon";

const Disc = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <circle cx="12" cy="12" r="3" />
        </IconContainer>
    );
};

Disc.displayName = "Disc";

export default Disc;
