import IconContainer, { IconProps } from "../icon";

const MoreHorizontal = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="1" />
            <circle cx="19" cy="12" r="1" />
            <circle cx="5" cy="12" r="1" />
        </IconContainer>
    );
};

MoreHorizontal.displayName = "MoreHorizontal";

export default MoreHorizontal;
