import IconContainer, { IconProps } from "../icon";

const Link2 = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M15 7h3a5 5 0 0 1 5 5 5 5 0 0 1-5 5h-3m-6 0H6a5 5 0 0 1-5-5 5 5 0 0 1 5-5h3" />
            <line x1="8" y1="12" x2="16" y2="12" />
        </IconContainer>
    );
};

Link2.displayName = "Link2";

export default Link2;
