import IconContainer, { IconProps } from "../icon";

const Pause = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <rect x="6" y="4" width="4" height="16" />
            <rect x="14" y="4" width="4" height="16" />
        </IconContainer>
    );
};

Pause.displayName = "Pause";

export default Pause;
