import IconContainer, { IconProps } from "../icon";

const Bold = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M6 4h8a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z" />
            <path d="M6 12h9a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z" />
        </IconContainer>
    );
};

Bold.displayName = "Bold";

export default Bold;
