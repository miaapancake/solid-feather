import IconContainer, { IconProps } from "../icon";

const GitMerge = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="18" cy="18" r="3" />
            <circle cx="6" cy="6" r="3" />
            <path d="M6 21V9a9 9 0 0 0 9 9" />
        </IconContainer>
    );
};

GitMerge.displayName = "GitMerge";

export default GitMerge;
