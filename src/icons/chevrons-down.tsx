import IconContainer, { IconProps } from "../icon";

const ChevronsDown = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="7 13 12 18 17 13" />
            <polyline points="7 6 12 11 17 6" />
        </IconContainer>
    );
};

ChevronsDown.displayName = "ChevronsDown";

export default ChevronsDown;
