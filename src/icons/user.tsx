import IconContainer, { IconProps } from "../icon";

const User = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />
            <circle cx="12" cy="7" r="4" />
        </IconContainer>
    );
};

User.displayName = "User";

export default User;
