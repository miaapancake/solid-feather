import IconContainer, { IconProps } from "../icon";

const ChevronsRight = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="13 17 18 12 13 7" />
            <polyline points="6 17 11 12 6 7" />
        </IconContainer>
    );
};

ChevronsRight.displayName = "ChevronsRight";

export default ChevronsRight;
