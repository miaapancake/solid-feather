import IconContainer, { IconProps } from "../icon";

const Rewind = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polygon points="11 19 2 12 11 5 11 19" />
            <polygon points="22 19 13 12 22 5 22 19" />
        </IconContainer>
    );
};

Rewind.displayName = "Rewind";

export default Rewind;
