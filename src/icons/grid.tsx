import IconContainer, { IconProps } from "../icon";

const Grid = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <rect x="3" y="3" width="7" height="7" />
            <rect x="14" y="3" width="7" height="7" />
            <rect x="14" y="14" width="7" height="7" />
            <rect x="3" y="14" width="7" height="7" />
        </IconContainer>
    );
};

Grid.displayName = "Grid";

export default Grid;
