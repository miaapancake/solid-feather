import IconContainer, { IconProps } from "../icon";

const Video = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polygon points="23 7 16 12 23 17 23 7" />
            <rect x="1" y="5" width="15" height="14" rx="2" ry="2" />
        </IconContainer>
    );
};

Video.displayName = "Video";

export default Video;
