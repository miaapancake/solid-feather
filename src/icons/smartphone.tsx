import IconContainer, { IconProps } from "../icon";

const Smartphone = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <rect x="5" y="2" width="14" height="20" rx="2" ry="2" />
            <line x1="12" y1="18" x2="12.01" y2="18" />
        </IconContainer>
    );
};

Smartphone.displayName = "Smartphone";

export default Smartphone;
