import IconContainer, { IconProps } from "../icon";

const Compass = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <polygon points="16.24 7.76 14.12 14.12 7.76 16.24 9.88 9.88 16.24 7.76" />
        </IconContainer>
    );
};

Compass.displayName = "Compass";

export default Compass;
