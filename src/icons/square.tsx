import IconContainer, { IconProps } from "../icon";

const Square = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
        </IconContainer>
    );
};

Square.displayName = "Square";

export default Square;
