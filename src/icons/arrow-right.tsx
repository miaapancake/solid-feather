import IconContainer, { IconProps } from "../icon";

const ArrowRight = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <line x1="5" y1="12" x2="19" y2="12" />
            <polyline points="12 5 19 12 12 19" />
        </IconContainer>
    );
};

ArrowRight.displayName = "ArrowRight";

export default ArrowRight;
