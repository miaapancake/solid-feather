import IconContainer, { IconProps } from "../icon";

const Bluetooth = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="6.5 6.5 17.5 17.5 12 23 12 1 17.5 6.5 6.5 17.5" />
        </IconContainer>
    );
};

Bluetooth.displayName = "Bluetooth";

export default Bluetooth;
