import IconContainer, { IconProps } from "../icon";

const ChevronRight = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="9 18 15 12 9 6" />
        </IconContainer>
    );
};

ChevronRight.displayName = "ChevronRight";

export default ChevronRight;
