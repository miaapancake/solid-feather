import IconContainer, { IconProps } from "../icon";

const Minus = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <line x1="5" y1="12" x2="19" y2="12" />
        </IconContainer>
    );
};

Minus.displayName = "Minus";

export default Minus;
