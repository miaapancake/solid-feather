import IconContainer, { IconProps } from "../icon";

const RotateCcw = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="1 4 1 10 7 10" />
            <path d="M3.51 15a9 9 0 1 0 2.13-9.36L1 10" />
        </IconContainer>
    );
};

RotateCcw.displayName = "RotateCcw";

export default RotateCcw;
