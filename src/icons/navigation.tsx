import IconContainer, { IconProps } from "../icon";

const Navigation = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polygon points="3 11 22 2 13 21 11 13 3 11" />
        </IconContainer>
    );
};

Navigation.displayName = "Navigation";

export default Navigation;
