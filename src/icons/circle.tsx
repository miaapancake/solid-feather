import IconContainer, { IconProps } from "../icon";

const Circle = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
        </IconContainer>
    );
};

Circle.displayName = "Circle";

export default Circle;
