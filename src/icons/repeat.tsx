import IconContainer, { IconProps } from "../icon";

const Repeat = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="17 1 21 5 17 9" />
            <path d="M3 11V9a4 4 0 0 1 4-4h14" />
            <polyline points="7 23 3 19 7 15" />
            <path d="M21 13v2a4 4 0 0 1-4 4H3" />
        </IconContainer>
    );
};

Repeat.displayName = "Repeat";

export default Repeat;
