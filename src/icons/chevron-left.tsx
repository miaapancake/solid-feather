import IconContainer, { IconProps } from "../icon";

const ChevronLeft = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="15 18 9 12 15 6" />
        </IconContainer>
    );
};

ChevronLeft.displayName = "ChevronLeft";

export default ChevronLeft;
