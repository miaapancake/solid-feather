import IconContainer, { IconProps } from "../icon";

const PlayCircle = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <polygon points="10 8 16 12 10 16 10 8" />
        </IconContainer>
    );
};

PlayCircle.displayName = "PlayCircle";

export default PlayCircle;
