import IconContainer, { IconProps } from "../icon";

const MapPin = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z" />
            <circle cx="12" cy="10" r="3" />
        </IconContainer>
    );
};

MapPin.displayName = "MapPin";

export default MapPin;
