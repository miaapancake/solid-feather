import IconContainer, { IconProps } from "../icon";

const Clock = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <polyline points="12 6 12 12 16 14" />
        </IconContainer>
    );
};

Clock.displayName = "Clock";

export default Clock;
