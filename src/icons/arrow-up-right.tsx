import IconContainer, { IconProps } from "../icon";

const ArrowUpRight = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <line x1="7" y1="17" x2="17" y2="7" />
            <polyline points="7 7 17 7 17 17" />
        </IconContainer>
    );
};

ArrowUpRight.displayName = "ArrowUpRight";

export default ArrowUpRight;
