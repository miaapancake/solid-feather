import IconContainer, { IconProps } from "../icon";

const ChevronsUp = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="17 11 12 6 7 11" />
            <polyline points="17 18 12 13 7 18" />
        </IconContainer>
    );
};

ChevronsUp.displayName = "ChevronsUp";

export default ChevronsUp;
