import IconContainer, { IconProps } from "../icon";

const CornerDownRight = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="15 10 20 15 15 20" />
            <path d="M4 4v7a4 4 0 0 0 4 4h12" />
        </IconContainer>
    );
};

CornerDownRight.displayName = "CornerDownRight";

export default CornerDownRight;
