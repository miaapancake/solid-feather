import IconContainer, { IconProps } from "../icon";

const Twitch = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M21 2H3v16h5v4l4-4h5l4-4V2zm-10 9V7m5 4V7" />
        </IconContainer>
    );
};

Twitch.displayName = "Twitch";

export default Twitch;
