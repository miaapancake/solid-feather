import IconContainer, { IconProps } from "../icon";

const MinusSquare = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
            <line x1="8" y1="12" x2="16" y2="12" />
        </IconContainer>
    );
};

MinusSquare.displayName = "MinusSquare";

export default MinusSquare;
