import IconContainer, { IconProps } from "../icon";

const Thermometer = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M14 14.76V3.5a2.5 2.5 0 0 0-5 0v11.26a4.5 4.5 0 1 0 5 0z" />
        </IconContainer>
    );
};

Thermometer.displayName = "Thermometer";

export default Thermometer;
