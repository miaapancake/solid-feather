import IconContainer, { IconProps } from "../icon";

const Check = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="20 6 9 17 4 12" />
        </IconContainer>
    );
};

Check.displayName = "Check";

export default Check;
