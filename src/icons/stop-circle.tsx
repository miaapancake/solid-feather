import IconContainer, { IconProps } from "../icon";

const StopCircle = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <rect x="9" y="9" width="6" height="6" />
        </IconContainer>
    );
};

StopCircle.displayName = "StopCircle";

export default StopCircle;
