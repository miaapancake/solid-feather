import IconContainer, { IconProps } from "../icon";

const Activity = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="22 12 18 12 15 21 9 3 6 12 2 12" />
        </IconContainer>
    );
};

Activity.displayName = "Activity";

export default Activity;
