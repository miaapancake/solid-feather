import IconContainer, { IconProps } from "../icon";

const Volume = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polygon points="11 5 6 9 2 9 2 15 6 15 11 19 11 5" />
        </IconContainer>
    );
};

Volume.displayName = "Volume";

export default Volume;
