import IconContainer, { IconProps } from "../icon";

const MinusCircle = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="12" r="10" />
            <line x1="8" y1="12" x2="16" y2="12" />
        </IconContainer>
    );
};

MinusCircle.displayName = "MinusCircle";

export default MinusCircle;
