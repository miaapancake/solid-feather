import IconContainer, { IconProps } from "../icon";

const Award = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <circle cx="12" cy="8" r="7" />
            <polyline points="8.21 13.89 7 23 12 20 17 23 15.79 13.88" />
        </IconContainer>
    );
};

Award.displayName = "Award";

export default Award;
