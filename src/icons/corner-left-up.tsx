import IconContainer, { IconProps } from "../icon";

const CornerLeftUp = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="14 9 9 4 4 9" />
            <path d="M20 20h-7a4 4 0 0 1-4-4V4" />
        </IconContainer>
    );
};

CornerLeftUp.displayName = "CornerLeftUp";

export default CornerLeftUp;
