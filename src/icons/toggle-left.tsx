import IconContainer, { IconProps } from "../icon";

const ToggleLeft = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <rect x="1" y="5" width="22" height="14" rx="7" ry="7" />
            <circle cx="8" cy="12" r="3" />
        </IconContainer>
    );
};

ToggleLeft.displayName = "ToggleLeft";

export default ToggleLeft;
