import IconContainer, { IconProps } from "../icon";

const ArrowDownLeft = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <line x1="17" y1="7" x2="7" y2="17" />
            <polyline points="17 17 7 17 7 7" />
        </IconContainer>
    );
};

ArrowDownLeft.displayName = "ArrowDownLeft";

export default ArrowDownLeft;
