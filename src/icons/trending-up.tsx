import IconContainer, { IconProps } from "../icon";

const TrendingUp = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="23 6 13.5 15.5 8.5 10.5 1 18" />
            <polyline points="17 6 23 6 23 12" />
        </IconContainer>
    );
};

TrendingUp.displayName = "TrendingUp";

export default TrendingUp;
