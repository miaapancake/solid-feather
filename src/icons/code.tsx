import IconContainer, { IconProps } from "../icon";

const Code = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="16 18 22 12 16 6" />
            <polyline points="8 6 2 12 8 18" />
        </IconContainer>
    );
};

Code.displayName = "Code";

export default Code;
