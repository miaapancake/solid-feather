import IconContainer, { IconProps } from "../icon";

const Edit3 = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <path d="M12 20h9" />
            <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z" />
        </IconContainer>
    );
};

Edit3.displayName = "Edit3";

export default Edit3;
