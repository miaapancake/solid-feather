import IconContainer, { IconProps } from "../icon";

const CornerUpRight = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <polyline points="15 14 20 9 15 4" />
            <path d="M4 20v-7a4 4 0 0 1 4-4h12" />
        </IconContainer>
    );
};

CornerUpRight.displayName = "CornerUpRight";

export default CornerUpRight;
