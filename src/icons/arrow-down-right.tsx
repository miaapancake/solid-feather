import IconContainer, { IconProps } from "../icon";

const ArrowDownRight = (props: IconProps) => {
    return (
        <IconContainer {...props}>
            <line x1="7" y1="7" x2="17" y2="17" />
            <polyline points="17 7 17 17 7 17" />
        </IconContainer>
    );
};

ArrowDownRight.displayName = "ArrowDownRight";

export default ArrowDownRight;
