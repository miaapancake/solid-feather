import { JSX } from "solid-js";

export type IconProps = JSX.SvgSVGAttributes<SVGSVGElement> & {
    size?: number;
};

export default function IconContainer({
    size = 24,
    color = "currentColor",
    children,
    ...rest
}: IconProps) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width={size}
            height={size}
            viewBox="0 0 24 24"
            fill="none"
            stroke={color}
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
            {...rest}
        >
            {children}
        </svg>
    );
}
