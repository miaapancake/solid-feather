# Solid Feather Icons

![npm version](https://img.shields.io/npm/v/solid-feather?style=for-the-badge&logo=npm&color=blue)
![npm downloads](https://img.shields.io/npm/dt/solid-feather?style=for-the-badge&logo=npm)

### What is solid-feather?

solid-feather is a solid wrapper for [feather icons](https://feathericons.com/).
An open source library of beautiful and consistent icons.

### Version: V4.29.0

### Installation:
    yarn add solid-feather

or

    npm i solid-feather

or

    pnpm install solid-feather

or

    bun install solid-feather

### Usage Example

```typescript
import { Activity } from "solid-feather";

function Component() {
    
    //...

    return (
        <h2>
            <Activity />
            Your Activity
        </h2>
    );

}

```
